import AboutMe from "./components/AboutMe";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Projects from "./components/Projects";
import TechStack from "./components/TechStack";
function App() {
  return (
    <>
    <Header></Header>
    <AboutMe></AboutMe>
    <Projects></Projects>
    <TechStack></TechStack>
    <Footer></Footer>
    </>

  );
}

export default App;
