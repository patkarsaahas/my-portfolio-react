import { Box, Heading, Text, Flex, Image } from '@chakra-ui/react'
import html from '../images/html.png';
import css from '../images/css.png';
import javascript from '../images/javascript.png';
import react from '../images/react.png';
import nodejs from '../images/nodejsDark.svg';
import expressjs from '../images/icons8-express-js-50.png';
import mongodb from '../images/mongodb.png';
import redux from '../images/redux.png';
import postman from '../images/postman-icon.png';
// import figma from '../images/figma.webp';

const TechStack = () => {
    return (
        <>
            <Heading size={'2xl'} justifyContent={'center'} alignItems={'center'} textAlign={'center'} mt={{base:'2', md:'5'}} mb={'3'}><Text bgGradient='linear(to-r, teal.900, teal.500, teal.100)' bgClip={'text'}>Tech Stack</Text></Heading>

            {/* <Box bgColor={'teal.800'} mx={'auto'} style={{ width: '101px', height: '610px' }}>
                <Image src={figma} alt="html-tech" boxSize={'100px'}
                />
                <Image src={html} alt="html-tech" boxSize={'100px'}
                    />
                <Image src={css} alt="css-tech" ml={'1'} />
                <Image ml={'1'} src={javascript} alt="react-tech" />
                <Image src={react} alt="react-tech" ml={'1'} mt={'1'} />
                <Image src={mongodb} alt="react-tech" mt='1' />
                <Image src={redux} alt="react-tech" mt='1'/>
            </Box> */}

            <Box>
                <Flex display={{ base: 'block', md: 'flex' }} justifyContent={{ base: 'space-between', md: 'space-evenly' }}>
                    <Box>
                        <Image src={html} alt="html-tech" boxSize={'50px'} display={{base:'none', md:'block'}}
                        />
                        <Text textAlign={{ base: 'center' }} fontFamily={'"Comfortaa", sans-serif;'} fontSize={{ base: '1.5rem', md: '2rem' }} bgGradient='linear(to-r, #e34c26, #f06529)' bgClip={'text'}>HTML</Text>
                    </Box>
                    <Box>
                        <Image src={css} alt="css-tech" ml={'1'} boxSize={'49px'} display={{base:'none', md:'block'}}/>
                        <Text textAlign={{ base: 'center' }} fontFamily={'"Inter", sans-serif;'} fontSize={{ base: '1.5rem', md: '2rem' }} bgGradient='linear(to-r, #264de4, #2965f1)' bgClip={'text'}>CSS</Text>
                    </Box>
                    <Box>
                        <Image ml={'1'} src={javascript} alt="react-tech" boxSize={'49px'} display={{base:'none', md:'block'}}/>

                        <Text textAlign={{ base: 'center' }} fontFamily={'"Inter", sans-serif;'} fontSize={{ base: '1.5rem', md: '2rem' }} bgGradient='linear(to-r, #f0db4f, #323330)' bgClip={'text'}>JAVASCRIPT</Text>
                    </Box>
                    <Box>
                        <Image src={react} alt="react-tech" ml={'1'} mt={'1'} boxSize={'45px'} display={{base:'none', md:'block'}}/>

                        <Text textAlign={{ base: 'center' }} fontFamily={'"Inter", sans-serif;'} fontSize={{ base: '1.5rem', md: '2rem' }} bgGradient='linear(to-r, #61dafb, #20232a)' bgClip={'text'}>REACT JS</Text>
                    </Box>
                    <Box>
                        <Image src={nodejs} alt='react-tech' w={'50px'} h={'50px'} display={{base:'none', md:'block'}}></Image>
                        <Text textAlign={{ base: 'center' }} fontFamily={'"Inter", sans-serif;'} fontSize={{ base: '1.5rem', md: '2rem' }} bgGradient='linear(to-r, #6cc24a, #333333)' bgClip={'text'}>NODE JS</Text>
                    </Box>
                    <Box>
                        <Image src={expressjs} alt='react-tech' w={'50px'} h={'50px'} display={{base:'none', md:'block'}}></Image>
                        <Text textAlign={{ base: 'center' }} fontFamily={'"Inter", sans-serif;'} fontSize={{ base: '1.5rem', md: '2rem' }} bgGradient='linear(to-r, #259dff, #353535)' bgClip={'text'}>EXPRESS JS</Text>
                    </Box>

                    <Box>
                        <Image src={mongodb} alt="react-tech" mt='1' w={'150px'} h={'50px'} display={{base:'none', md:'block'}}/>

                        <Text textAlign={{ base: 'center' }} fontFamily={'"Inter", sans-serif;'} fontSize={{ base: '1.5rem', md: '2rem' }} bgGradient='linear(to-r,#3F3E42, #3FA037)' bgClip={'text'}>MONGODB</Text>
                    </Box>

                    <Box>
                        <Image src={redux} alt="react-tech" mt='1' boxSize={'50px'} display={{base:'none', md:'block'}}/>
                        <Text textAlign={{ base: 'center' }} fontFamily={'"Inter", sans-serif;'} fontSize={{ base: '1.5rem', md: '2rem' }} bgGradient='linear(to-r, #764abc, #3F3E42)' bgClip={'text'}>REDUX</Text>
                    </Box>

                    <Box>
                        <Image src={postman} alt='react-tech' w={'50px'} h={'50px'} display={{base:'none', md:'block'}}></Image>

                        <Text textAlign={{ base: 'center' }} fontFamily={'"Inter", sans-serif;'} fontSize={{ base: '1.5rem', md: '2rem' }} bgGradient='linear(to-r, #ef5b25, #3F3E42)' bgClip={'text'}>REST API</Text>
                    </Box>
                </Flex>
            </Box>
        </>
    )
}

export default TechStack