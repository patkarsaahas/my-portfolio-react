import { Box, Flex, Heading, Link, Text } from '@chakra-ui/react'
import React from 'react'
import html from '../images/html.png';
import css from '../images/css.png';
import javascript from '../images/javascript.png';
import react from '../images/react.png';
import github from '../images/github.png';
import gitlab from '../images/gitlab.png';
const Projects = () => {
    return (
        <>
            <Heading size={'2xl'} justifyContent={'center'} alignItems={'center'} textAlign={'center'} mb={'3'}><Text bgGradient='linear(to-r, teal.900, teal.500, teal.100)' bgClip={'text'}>Projects</Text></Heading>
            <Flex mb='5' direction={{ base: 'column', md: 'row' }}>
                <Box bgColor={'teal.900'} width={{ base: 'full', md: '500px' }} h={{ base: 'full', md: '250px' }} rounded={'md'} mx={{ base: 'auto', md: 'auto' }} my={{ base: '3', md: '0' }} p='3'>
                    <Link _hover={{ textDecoration: 'none' }} href="https://rststore-frontend.onrender.com" color={'white'} target='_blank'><Text fontSize={{ base: '16px', md: '18px' }} fontFamily={'"Inter", sans-serif;'}>Live Preview: RST Store</Text></Link>
                    <Text fontSize={{ base: '14px', md: '14px' }} color={'whitesmoke'} mt='2' fontFamily={'"Inter", sans-serif;'}>A fully functional single page ecom-website with<br /> user authentication, admin panel and <br />payment gateway.</Text>
                    <Text fontSize={{ base: '16px', md: '18px' }} color={'white'} className='text-white pl-5' mt={'3'} fontFamily={'"Inter", sans-serif;'}>Tech Stack:</Text>
                    <Flex mt='2'>
                        <img src={html} alt="html-tech" style={{ width: "auto", height: "60px" }}
                            class="html-tech" />
                        <img src={css} alt="css-tech" style={{ width: '50px', height: '50px' }} class="css-tech" />
                        <img src={javascript} alt="react-tech"
                            style={{ marginLeft: '10px', width: '50px', height: '50px' }} class="react-tech" />
                        <img src={react} alt="react-tech"
                            style={{ marginLeft: '10px', width: '50px', height: '50px' }} class="react-tech" />
                        <a href="https://gitlab.com/patkarsaahas/rststore" target='_blank' rel='noreferrer'><img src={gitlab}
                            alt="github-tech" style={{ marginLeft: '10px', width: "auto", height: "50px" }}
                            class="github-tech" /></a>
                    </Flex>

                </Box>

                <Box width={{ base: 'full', md: '500px' }} h={{ base: 'full', md: '250px' }} bgColor={'teal.900'} rounded={'md'} p='3' mx={{ base: 'auto', md: 'auto' }} my={{ base: '3', md: '0' }}>
                    <Link _hover={{ textDecoration: 'none' }} href="https://vinayakinfra.onrender.com" color={'white'} target='_blank'><Text fontFamily={'"Inter", sans-serif;'}>Live Preview: VinayakInfra Website</Text></Link>
                    <Text fontSize={{ base: '14px', md: '14px' }} color={'white'} mt='2' fontFamily={'"Inter", sans-serif;'}>A fully functional single page construction website<br /> with download brochure feature and fast<br /> response time in switching between the pages.</Text>
                    <Text fontSize={{ base: '16px', md: '18px' }} color={'white'} className='text-white pl-5' mt='3' fontFamily={'"Inter", sans-serif;'}>Tech Stack:</Text>
                    <Flex mt='2'>
                        <img src={html} alt="html-tech" style={{ width: "auto", height: "60px" }}
                            class="html-tech" />
                        <img src={css} alt="css-tech" style={{ width: '50px', height: '50px' }} class="css-tech" />
                        <img src={javascript} alt="react-tech"
                            style={{ marginLeft: '10px', width: '50px', height: '50px' }} class="react-tech" />
                        <img src={react} alt="react-tech"
                            style={{ marginLeft: '10px', width: '50px', height: '50px' }} class="react-tech" />
                        <a href="https://github.com/Saahas12/vinayakinfra" target='_blank' rel='noreferrer'><img src={github}
                            alt="github-tech" width="50px" height="50px" style={{ marginLeft: '10px' }}
                            class="github-tech" /></a>
                    </Flex>

                </Box>

                <Box width={{ base: 'full', md: '500px' }} h={{ base: 'full', md: '250px' }} bgColor={'teal.900'} rounded={'md'} p='3' mx={{ base: 'auto', md: 'auto' }} my={{ base: '3', md: '0' }}>
                    <Link _hover={{ textDecoration: 'none' }} href="https://blog-new-frontend.onrender.com" color={'white'} target='_blank'><Text fontFamily={'"Inter", sans-serif;'}>Live Preview: Blog Website</Text></Link>
                    <Text fontSize={{ base: '16px', md: '14px' }} color={'white'} mt='2' fontFamily={'"Inter", sans-serif;'}>Developed a website with several blogs that users can add, modify, and read about other people's
                        blogs.<br /> To create, remove, and edit their own blogs, users must authenticate.<br /> Comments can also be added by users to particular blogs. <br />A user's personal blog can be downloaded.</Text>
                    <Text fontSize={{ base: '16px', md: '18px' }} color={'white'} className='text-white pl-5' mt='3' fontFamily={'"Inter", sans-serif;'}>Tech Stack:</Text>
                    <Flex mt='2'>
                        <img src={html} alt="html-tech" style={{ width: "auto", height: "60px" }}
                            class="html-tech" />
                        <img src={css} alt="css-tech" style={{ width: '50px', height: '50px' }} class="css-tech" />
                        <img src={javascript} alt="react-tech"
                            style={{ marginLeft: '10px', width: '50px', height: '50px' }} class="react-tech" />
                        <img src={react} alt="react-tech"
                            style={{ marginLeft: '10px', width: '50px', height: '50px' }} class="react-tech" />
                        <a href="https://gitlab.com/patkarsaahas/blog-new" target='_blank' rel='noreferrer'><img src={gitlab}
                            alt="github-tech" style={{ marginLeft: '10px', width: "auto", height: "50px" }}
                            class="github-tech" /></a>
                    </Flex>

                </Box>
            </Flex>
        </>
    )
}

export default Projects