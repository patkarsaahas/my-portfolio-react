import React from 'react'
import { Box, Grid, Heading, Image, Text } from '@chakra-ui/react'
import profile from '../images/Profile_Photo.jpg';
import '../css/AboutMe.css'
const AboutMe = () => {
    return (
        <>
            <Heading size={'2xl'} justifyContent={'center'} alignItems={'center'} textAlign={'center'} mt={'5'} mb={'3'}>
                <Text bgGradient='linear(to-r, teal.900, teal.500, teal.100)' bgClip={'text'}>
                    About Me
                </Text>
            </Heading>
            <Grid templateRows={{ base: '1fr', md: 'repeat(2, 1fr)' }} gap={0} mb='3'>
                <Box textAlign="center" ml={'auto'} mr={'auto'} mt='3'>
                    <Image src={profile} borderRadius='full' boxSize={{ base: '200px', md: '350px' }} alt='profile-photo' />
                </Box>
                <Box textAlign={{ base: 'center', md: 'center' }} alignItems={'center'} justifyItems={'center'} my={'auto'} ml={'auto'} mr={'auto'}>
                    <Text fontSize={{ base: '18px', md: '24px' }} color='Purple.500'>
                        HI FOLKS<span className='wave'>👋</span>, I am Saahas Hemant Patkar, a Full Stack Developer, inherently inquisitive. I am equipped with the ability to accept criticism and thrive on collaborative teamwork.
                    </Text>
                </Box>
            </Grid>

            {/* <Heading size={'2xl'} justifyContent={'center'} alignItems={'center'} textAlign={'center'} mt={'1'} mb={'3'}><Text bgGradient='linear(to-r, teal.900, teal.500, teal.100)' bgClip={'text'}>About Me</Text></Heading>
            <Grid templateColumns='repeat(2, 1fr)' mb='3'>
                <Box mt={'3'} ml={'50%'} alignItems={'center'} justifyItems={'center'}>
                    <Image src={profile} borderRadius='full'
                        boxSize='300px' alt='profile-photo'>
    
                        </Image>
                </Box>

                <Box my={'auto'} alignItems={'center'} justifyItems={'center'} textAlign={'justify'} ml={'3'} mr={'10%'}>
                    <Text fontSize='24px' color='Purple.500'>
                    HI FOLKS<span className='wave'>👋</span>, I am Saahas Hemant Patkar, a Full Stack Developer, inherently inquisitive. I am equipped with the ability to accept criticism and thrive on collaborative teamwork.
                    </Text>
                </Box>
            </Grid> */}

        </>
    )
}

export default AboutMe