import { Heading, Text } from '@chakra-ui/react'
import React from 'react'

const Footer = () => {
    return (
        <>
            <Heading mt={'5'} size='sm' textAlign={'center'} color={'teal.500'}><Text>All rights reserved ©️.{new Date().getFullYear()}</Text></Heading>
        </>
    )
}

export default Footer