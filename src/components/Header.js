import React from 'react'
import { Box, Flex, Link } from '@chakra-ui/react'
import { FaInstagram } from 'react-icons/fa'
import { BiLogoGmail } from "react-icons/bi";
import { FaSquareGitlab } from "react-icons/fa6";
import { GoDownload } from "react-icons/go";
import { FaLinkedin } from "react-icons/fa";
const Header = () => {
    return (
        <>
            <Box display={{base: 'block', sm:'block'}}>
                <Flex justifyContent={'center'} gap={'5'} alignItems={'center'}>
                    <Box><Link _hover={{textDecoration:'none'}} href='https://www.instagram.com/saahaspatkar/' target='_blank'><FaInstagram/></Link></Box>
                    <Box><Link _hover={{textDecoration:'none'}} href="mailto:patkarsaahas@gmail.com" target='_blank'><BiLogoGmail/></Link></Box>
                    <Box><Link _hover={{textDecoration:'none'}} href="https://www.linkedin.com/in/saahas-patkar-21798b1b2/" target="_blank"><FaLinkedin/></Link></Box>
                    <Box><Link _hover={{textDecoration:'none'}} href='https://gitlab.com/patkarsaahas' target='_blank'><FaSquareGitlab/></Link></Box>
                    <Box justifyContent={'right'} bgColor={'yellow.300'} p={'1'} mt={'1'} rounded={'md'}>
                        <Flex>
                        <Link _hover={{textDecoration:'none'}} href='SAAHAS_PATKAR_NEW RESUME-1 (1).pdf' download='SAAHAS_PATKAR_NEW RESUME-1 (1).pdf' color={'black'}>Checkout My Resume</Link>
                        <GoDownload style={{alignItems:'center', justifyContent:'center', marginTop:'auto', marginBottom:'auto'}}></GoDownload>
                        </Flex>
                    </Box>
                </Flex>
            </Box>
        </>
    )
}

export default Header